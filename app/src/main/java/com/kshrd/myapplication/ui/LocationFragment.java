package com.kshrd.myapplication.ui;

import android.Manifest;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;


import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.kshrd.myapplication.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class LocationFragment extends Fragment implements
        SearchView.OnQueryTextListener, View.OnClickListener
{

    GoogleMap map;
    SupportMapFragment mapFragment;
    FusedLocationProviderClient client;
    SearchView searchView;
    Button btnAtm, btnBank, btnHospital, btnTheater, btnRestaurant;
    String[] placeTypeList = {"atm", "bank", "hospital", "movie_theater", "restaurant"};
    double currentLat=0, currentLong=0;
    ImageView direction;
    LatLng searchLocation;
    LatLng currentLocation;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_near_by, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.google_map);
        searchView = root.findViewById(R.id.svLocation);
        btnAtm = root.findViewById(R.id.btn_atm);
        btnBank = root.findViewById(R.id.btn_bank);
        btnHospital = root.findViewById(R.id.btn_hospital);
        btnTheater = root.findViewById(R.id.btn_theater);
        btnRestaurant = root.findViewById(R.id.btn_restaurant);
        direction = root.findViewById(R.id.get_direction);
        direction.setVisibility(View.GONE);

        btnAtm.setOnClickListener(this);
        btnBank.setOnClickListener(this);
        btnHospital.setOnClickListener(this);
        btnTheater.setOnClickListener(this);
        btnRestaurant.setOnClickListener(this);
        direction.setOnClickListener(this);

        int searchCloseButtonId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButton = (ImageView) this.searchView.findViewById(searchCloseButtonId);
        // Set on click listener
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                direction.setVisibility(View.GONE);
                if (map != null){
                    map.clear();
                }
                getCurrentLocation();
            }
        });

        client = LocationServices.getFusedLocationProviderClient(getContext());
        if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            getCurrentLocation();
            searchView.setOnQueryTextListener(this);
        }else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
        }
        return root;
    }

    //method to get current location
    List<Address> addresses = null;
    LatLng latLng;
    private void getCurrentLocation() {

        if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            Task<Location> task = client.getLastLocation();
            task.addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if(location != null){

                        Geocoder geocoder = new Geocoder(getContext());
                        try {
                            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if( addresses.size() != 0){
                            Address address = addresses.get(0);
                            currentLat = address.getLatitude();
                            currentLong = address.getLongitude();
                            latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        }
                        mapFragment.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap googleMap) {
                                map = googleMap;
                                MarkerOptions options = new MarkerOptions().position(latLng).title("Device Location");
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                                googleMap.addMarker(options);
                            }
                        });
                    }
                    else{
                        Toast.makeText(getContext(), "Can not detect current location...", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    // Permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100){
            if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ){
                getCurrentLocation();
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
         String location = searchView.getQuery().toString();
        List<Address> addresses = null;
        if (searchLocation != null || !searchLocation.equals("")){
            Geocoder geocoder = new Geocoder(getContext());
            try {
                addresses = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if( addresses.size() != 0){
                Address address = addresses.get(0);
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        map = googleMap;
                        MarkerOptions options = new MarkerOptions().position(latLng).title(location);
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                        map.addMarker(options);
                    }
                });
            }
            else {
                Toast.makeText(getContext(), searchLocation + " is not found", Toast.LENGTH_SHORT).show();
            }
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onClick(View v) {
        String nearByUrl = null;
        switch (v.getId()){
            case R.id.btn_atm:
                nearByUrl = near_byLocation(placeTypeList[0]);
                break;
            case  R.id.btn_bank:
                nearByUrl = near_byLocation(placeTypeList[1]);
                break;
            case R.id.btn_hospital:
                nearByUrl = near_byLocation(placeTypeList[2]);
                break;
            case R.id.btn_theater:
                nearByUrl = near_byLocation(placeTypeList[3]);
                break;
            case R.id.btn_restaurant:
                nearByUrl = near_byLocation(placeTypeList[4]);
                break;
            case R.id.get_direction :

                break;
            default:
                Toast.makeText(getContext(), "Can not load places..." , Toast.LENGTH_SHORT).show();
                break;
        }
        new PlaceTask().execute(nearByUrl);
    }

    private void displayTrack(String currentLocation, String searchLocation) {

    }

    public String near_byLocation(String type){
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json" +
                "?location=" + currentLat + "," + currentLong +
                "&radius=5000" +
                "&types=" + type +
                "&sensor=true" +
                "&key=" + getResources().getString(R.string.mapApi);
        return url;
    }

    private class PlaceTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... strings) {
            String data = null;
            try {
                 data = downloadUrl(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
            new ParserTask().execute(s);
        }
    }

    private String downloadUrl(String string) throws IOException {
        URL url = new URL(string);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.connect();
        InputStream stream = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder builder = new StringBuilder();
        String line = "";
        while ((line = reader.readLine()) != null){
            builder.append(line);
        }

        String data = builder.toString();
        reader.close();

        return data;

    }

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        @Override
        protected List<HashMap<String, String>> doInBackground(String... strings) {
            JsonParser jsonParser = new JsonParser();
            List<HashMap<String, String>> mapList = null;
            JSONObject object = null;
            try {
                Log.d( "doInBackground: ", strings[0]);
                object = new JSONObject(strings[0]);
                mapList = jsonParser.parseResult(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return mapList;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> hashMaps) {
            for (int i=0; i<hashMaps.size(); i++){
                HashMap<String, String> hashMapList = hashMaps.get(i);
                double lat = Double.parseDouble(hashMapList.get("lat"));
                double lng = Double.parseDouble(hashMapList.get("lng"));
                String name = hashMapList.get("name");
                if(map != null){
                    map.clear();
                }
                LatLng latLng = new LatLng(lat,lng);
                MarkerOptions options = new MarkerOptions().position(latLng).title(name);
                map.addMarker(options);
            }
        }
    }

    @Override
    public void onAttachFragment(@NonNull Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }
}
